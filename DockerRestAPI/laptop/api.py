# Laptop Service

from flask import Flask
from flask_restful import Resource, Api
from flask import request
import pymongo
from pymongo import MongoClient

# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient('db', 27017)
db = client.tododb

class all(Resource):

    def get(self):
        n = request.args.get('top')
        if n == None:
            top = 50

        else:
            top = int(n)
        
        _items = db.tododb.find().sort([("open_t",pymongo.ASCENDING), ("close_t",pymongo.ASCENDING)]).limit(top)
        list_of_items = [item for item in _items]

        app.logger.debug("Items")
        app.logger.debug(list_of_items)

        dict_of_items = {}
        open_time = []
        close_time = []

        for i in list_of_items:
            open_time.append(i["open_t"])
            close_time.append(i["close_t"])

        app.logger.debug("Open")
        app.logger.debug(open_time)

        app.logger.debug("Close")
        app.logger.debug(close_time)

        dict_of_items["open_t"] = open_time
        dict_of_items["close_t"] = close_time
        return dict_of_items

api.add_resource(all, "/listAll")

class all_of_csv(Resource):

    def get(self):
        n = request.args.get('top')
        if n == None:
            top = 50
        else:
            top = int(n)

        _items = db.tododb.find().sort([("open_t",pymongo.ASCENDING), ("close_t",pymongo.ASCENDING)]).limit(top)
        list_of_items = [item for item in _items]

        str_of_csv = ""
        for i in list_of_items:
            str_of_csv += i["open_t"]+", "
            str_of_csv += i["close_t"]+", "

        return str_of_csv
            
api.add_resource(all_of_csv, "/listAll/csv")

class csv_is_open(Resource):
    
    def get(self):
        n = request.args.get('top')
        if n == None:
            top = 50
        else:
            top = int(n)
 
        _items = db.tododb.find().sort("open_t",pymongo.ASCENDING).limit(top)
        list_of_items = [item for item in _items]
            
        str_of_csv_open = ""
        for i in list_of_items:
            str_of_csv_open += i["open_t"]+", "

        return str_of_csv_open
api.add_resource(csv_is_open, "/listOpenOnly/csv")

class csv_is_close(Resource):
        
    def get(self):
        n = request.args.get('top')
        if n == None:
            top = 50
        else:
            top = int(n)
 
        _items = db.tododb.find().sort("close_t",pymongo.ASCENDING).limit(top)
        list_of_items = [item for item in _items]
                 
        str_of_csv_close = ""
        for i in list_of_items:
            str_of_csv_close += i["close_t"]+", "

        return str_of_csv_close
api.add_resource(csv_is_close, "/listCloseOnly/csv")        

class all_of_json(Resource):
    def get(self): 
        n = request.args.get('top')
        if n == None:
            top = 50
        else:
            top = int(n)
 
        _items = db.tododb.find().sort([("open_t",pymongo.ASCENDING), ("close_t",pymongo.ASCENDING)]).limit(top)
        list_of_items = [item for item in _items]
            
        dict_of_items = {}
        open_time = []
        close_time = []

        for i in list_of_items:
            open_time.append(i["open_t"])
            close_time.append(i["close_t"])

        dict_of_items["open_t"] = open_time
        dict_of_items["close_t"] = close_time

        return dict_of_items

api.add_resource(all_of_json, "/listAll/json")

class json_is_open(Resource):

    def get(self):
        n = request.args.get('top')
        if n == None:
            top = 50
        else:
            top = int(n)

        _items = db.tododb.find().sort("open_t",pymongo.ASCENDING).limit(top)
        list_of_items = [item for item in _items]

        dict_of_items = {}
        open_time = []
        close_time = []

        for i in list_of_items:
            open_time.append(i["open_t"])

        dict_of_items["open_t"] = open_time

        return dict_of_items 

api.add_resource(json_is_open, "/listOpenOnly/json")

class json_is_close(Resource):

    def get(self):
        n = request.args.get('top')
        if n == None:
            top = 50
        else:
            top = int(n)

        _items = db.tododb.find().sort("close_t",pymongo.ASCENDING).limit(top)
        list_of_items = [item for item in _items]

        dict_of_items = {}
        open_time = []
        close_time = []

        for i in list_of_items:
            close_time.append(i["close_t"])

        dict_of_items["close_t"] = close_time

        return dict_of_items 

api.add_resource(json_is_close, "/listCloseOnly/json")


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
